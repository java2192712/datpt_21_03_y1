package com.example.datpt_21_03_y1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Datpt2103Y1Application {

	public static void main(String[] args) {
		SpringApplication.run(Datpt2103Y1Application.class, args);
	}

}
