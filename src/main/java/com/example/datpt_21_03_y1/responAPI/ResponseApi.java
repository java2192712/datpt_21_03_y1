package com.example.datpt_21_03_y1.responAPI;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class ResponseApi {
    private String message;
    private Map<String, List<?>> content;
    private int countObject;
}
