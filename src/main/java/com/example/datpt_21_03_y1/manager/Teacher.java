package com.example.datpt_21_03_y1.manager;

public class Teacher extends Person{
    private float namNgheNghiep;
    private String tinhTrangHonNhan;

    public float getNamNgheNghiep() {
        return namNgheNghiep;
    }

    public String getTinhTrangHonNhan() {
        return tinhTrangHonNhan;
    }

    public void setNamNgheNghiep(float namNgheNghiep) {
        this.namNgheNghiep = namNgheNghiep;
    }

    public void setTinhTrangHonNhan(String tinhTrangHonNhan) {
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }

    public Teacher(String ten, int tuoi, String gioiTinh, float namNgheNghiep, String tinhTrangHonNhan) {
        super(ten, tuoi, gioiTinh);
        this.namNgheNghiep = namNgheNghiep;
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }

    public Teacher(String ten, int tuoi, String gioiTinh) {
        super(ten, tuoi, gioiTinh);
    }

    public Teacher(float namNgheNghiep, String tinhTrangHonNhan) {
        this.namNgheNghiep = namNgheNghiep;
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }

    public Teacher() {}

    @Override
    public String printHello(){
        return "Hello, I am a Teacher";
    }

    // đây không phải là Override do class preson để hàm là private chỉ là tạo ra phương thức trùng tên bên class com
    public void hello(){
        System.out.println("Cô giáo tốt");
    }

    public String printHello(String ten, int tuoi){
        return  "Hello, I am a Teacher, my name: "+ten+ " i am "+tuoi+" years old";
    }

    public String printHello(int tuoi){
        return  "Hello, I am a Teacher,  i am "+ tuoi+" years old";
    }

}
