package com.example.datpt_21_03_y1.service;

import com.example.datpt_21_03_y1.manager.Student;
import com.example.datpt_21_03_y1.responAPI.ResponseApi;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class StudentService {
    private final List<Student> students = new ArrayList<>();
    public ResponseApi getStudent(){
        Map<String, List<?>> dataMap = new HashMap<>();
        dataMap.put("students", students);
        int countObject = students.size();
        return new ResponseApi("Xem Student thành công",dataMap,countObject);
    }

    public ResponseApi addStudent(Student student){
        students.add(student);
        return new ResponseApi("Them Student thanh công",null, 0);
    }

    public ResponseApi updateStudent(int index, Student student){
        students.set(index, student);
        return new ResponseApi("Cập nhận Student thành công",null, 0);
    }

    public ResponseApi deleteStudent(int index){
        students.remove(index);
        return new ResponseApi("Xóa Student thành công",null, 0);
    }

}
