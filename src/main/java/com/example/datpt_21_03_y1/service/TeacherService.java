package com.example.datpt_21_03_y1.service;

import com.example.datpt_21_03_y1.manager.Student;
import com.example.datpt_21_03_y1.manager.Teacher;
import com.example.datpt_21_03_y1.responAPI.ResponseApi;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TeacherService {
    private List<Teacher> teachers = new ArrayList<>();

    public ResponseApi getTeacher(){
        Map<String, List<?>> dataMap = new HashMap<>();
        dataMap.put("teachers", teachers);
        int countObject = teachers.size();
        return new ResponseApi("Xem Teacher thành công",dataMap,countObject);
    }

    public ResponseApi addTeacher(Teacher teacher){
        teachers.add(teacher);
        return new ResponseApi("Thêm Teacher thành công",null,0);
    }

    public ResponseApi updateTeacher(int index, Teacher teacher){
        teachers.set(index, teacher);
        return new ResponseApi("Cập nhận Student thành công",null, 0);
    }

    public ResponseApi deleteTeacher(int index){
        teachers.remove(index);
        return new ResponseApi("Xóa Student thành công",null, 0);
    }
}
