package com.example.datpt_21_03_y1.controller;

import com.example.datpt_21_03_y1.manager.Student;
import com.example.datpt_21_03_y1.manager.Teacher;
import com.example.datpt_21_03_y1.responAPI.ResponseApi;
import com.example.datpt_21_03_y1.service.StudentService;
import com.example.datpt_21_03_y1.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {
    private final StudentService studentService;
    private final TeacherService teacherService;

    @GetMapping("/getStudent")
    public ResponseEntity<ResponseApi> getStudent() {
        return new ResponseEntity<>(studentService.getStudent(), HttpStatus.OK);
    }

    @PostMapping("/addStudent")
    public ResponseEntity<ResponseApi> addStudent(@RequestBody Student student){
        return new ResponseEntity<>(studentService.addStudent(student), HttpStatus.OK);
    }
    @PutMapping("/updateStudent/{index}")
    public ResponseEntity<ResponseApi> updateStudent(@PathVariable int index  ,@RequestBody Student student){
        return new ResponseEntity<>(studentService.updateStudent(index, student), HttpStatus.OK);
    }


    @DeleteMapping("/deleteStudent/{index}")
    public ResponseEntity<ResponseApi> deleteStudent(@PathVariable int index){
        return new ResponseEntity<>(studentService.deleteStudent(index), HttpStatus.OK);
    }


    @GetMapping("/getTeacher")
    public ResponseEntity<ResponseApi> getTeacher() {
        return new ResponseEntity<>(teacherService.getTeacher(), HttpStatus.OK);
    }

    @PostMapping("/addTeacher")
    public ResponseEntity<ResponseApi> addTeacher(@RequestBody Teacher teacher){
        return new ResponseEntity<>(teacherService.addTeacher(teacher), HttpStatus.OK);
    }

    @PutMapping("/updateTeacher/{index}")
    public ResponseEntity<ResponseApi> updateTeacher(@PathVariable int index  ,@RequestBody Teacher teacher){
        return new ResponseEntity<>(teacherService.updateTeacher(index, teacher), HttpStatus.OK);
    }


    @DeleteMapping("/deleteTeacher/{index}")
    public ResponseEntity<ResponseApi> deleteTeacher(@PathVariable int index){
        return new ResponseEntity<>(teacherService.deleteTeacher(index), HttpStatus.OK);
    }


}
